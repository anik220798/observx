```

  ██████╗  ██████╗  ███████╗ ███████╗ ██████╗  ██╗   ██╗ ██╗  ██╗
 ██╔═══██╗ ██╔══██╗ ██╔════╝ ██╔════╝ ██╔══██╗ ██║   ██║ ╚██╗██╔╝
 ██║   ██║ ██████╔╝ ███████╗ █████╗   ██████╔╝ ██║   ██║  ╚███╔╝
 ██║   ██║ ██╔══██╗ ╚════██║ ██╔══╝   ██╔══██╗ ╚██╗ ██╔╝  ██╔██╗
 ╚██████╔╝ ██████╔╝ ███████║ ███████╗ ██║  ██║  ╚████╔╝  ██╔╝ ██╗
  ╚═════╝  ╚═════╝  ╚══════╝ ╚══════╝ ╚═╝  ╚═╝   ╚═══╝   ╚═╝  ╚═╝

```


# ObservX :zap:

### Tini-Tiny Observer creating library. Not even sure it achievs that goal but surely does something :laughing:


## Installation 📂

```bash
$ npm i --save observx
```

## Usage 📑 

```javascript

const { subscribe } = require("observx")

var streamedData = subscribe([12, 323, 233], res => {
  console.log("changed");
});
console.log(streamedData.currentData()); // [12, 323, 233]
streamedData.changeWith([2]); 
console.log(streamedData.currentData()); // [2]


```

## API 💊

### `subscribe(data[,onchangeCB])`
the `subscribe` method takes one required argument which is the **observables** or the data and one **optional** callback which is called everytime any changes happens to the observer. It returns a **observer**

The callback `onchangeCB` will give a response argument which is a object consisting the data and the type of the data
```javascript
{ data, typeOfData: typeof data }
```


### `subcribe().changeWith(newData)`
this method takes a argument with which it will change the existing data. It will return the newdata which is replaced with

### `subcribe().currentData()`
It will simply return the current data (observables). 



### Note 🚧

Still Need some refactoring and fixing for the APIs and the `subscribe` method 🎓

Dont use it in production - Use it on your own risk ⚠️ 


