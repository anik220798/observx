
export type onChangeCBType = {
    (logs?: Object): void;
  };

export interface SubscribeI<T> {
    changeWith(data: T): void;
    currentData(): T;
  }
