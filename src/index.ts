

/*

  ██████╗  ██████╗  ███████╗ ███████╗ ██████╗  ██╗   ██╗ ██╗  ██╗
 ██╔═══██╗ ██╔══██╗ ██╔════╝ ██╔════╝ ██╔══██╗ ██║   ██║ ╚██╗██╔╝
 ██║   ██║ ██████╔╝ ███████╗ █████╗   ██████╔╝ ██║   ██║  ╚███╔╝
 ██║   ██║ ██╔══██╗ ╚════██║ ██╔══╝   ██╔══██╗ ╚██╗ ██╔╝  ██╔██╗
 ╚██████╔╝ ██████╔╝ ███████║ ███████╗ ██║  ██║  ╚████╔╝  ██╔╝ ██╗
  ╚═════╝  ╚═════╝  ╚══════╝ ╚══════╝ ╚═╝  ╚═╝   ╚═══╝   ╚═╝  ╚═╝

Work In Progress
Trying to create a Tini-Tiny replica
Not even sure if its with the same concept or not

*/

import {
  onChangeCBType,
  SubscribeI,
} from "./types";
// tslint:disable: indent

export const subscribe = < T > (
  data: T,
  onChangeCB: onChangeCBType = () => null,
): SubscribeI < T > => ({
  changeWith: (newdata) => {

    switch (typeof data) {
      case "object": {
        if (data instanceof Array && newdata instanceof Array) {
          onChangeCB({
            data,
            typeOfData: typeof data,
          });
          return (data = newdata);
        } else {
          onChangeCB({
            data,
            typeOfData: typeof data,
          });
          return {
            ...data,
            ...newdata,
          };
        }
        break;
      }
      case "string": {
        return (data = newdata);
        break;
      }

      /*
        Need more of these switch cases to cover maximum
        of the types
      */
      default:
        return (data = newdata);
        break;
    }
  }, // data = newdata,
  currentData: () => data,
});
