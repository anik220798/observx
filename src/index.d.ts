import { onChangeCBType, SubscribeI } from "./types";
export declare const subscribe: <T>(data: T, onChangeCB?: onChangeCBType) => SubscribeI<T>;
