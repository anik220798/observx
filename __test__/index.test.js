const {
  subscribe
} = require("../src/index");

describe("Testing for the subscribe().currentData() ", () => {
  it("It should return the same array (data) ", (done) => {
	expect(
		subscribe([1, 2, 3])
	.currentData()
	)
	.toEqual([1, 2, 3]);
 done();
  });
});

describe("Testing for the subscribe().changeWith", () => {
  it("It should return the same array (data) ", (done) => {
	expect(
		subscribe([1, 2, 3])
  .changeWith([3, 2, 1])
	)
	.toEqual([3, 2, 1]);
 done();
  });
});
